# Run a heuristic for a TTP instance

## Prerequisites:
- java 1.7+
- a linux OS / Windows will work for most heuristics



## Command:
java -jar ttplab-bbox.jar <algo_name> <instance_name> <init_sol_filename> <out_sol_filename>

- algo_name : heuristic to run, 7 blackboxes are available for now:
search:             kpsa, kpbf, tsp2opt, obj
disruptive ops:     otspswap, otsp2opt, otsp4opt
initialize:         init

- instance_name : TTP instance name, eg. eil76_n750_uncorr_10.ttp

- init_sol_filename [optional] : initial solution file name.
    
If not provided, a default solution will be generated automatically

- out_sol_filename [optional] : output solution filename.
    
By default, the solution will be displayed in terminal



## Output:
- objective value and runtime
- best found solution



## Examples:


### Calculate objective:
java -jar ttplab-bbox.jar obj berlin52_n51_uncorr_01.ttp berlin52sol.txt


### call the KP-BF heuristic and save resulting solution in a text file:
java -jar ttplab-bbox.jar kpbf berlin52_n51_uncorr_01.ttp berlin52sol.txt berlin52outsol.txt


### initialize a solution:
java -jar ttplab-bbox.jar init berlin52_n51_uncorr_01.ttp none abc.txt


# Algorithms

- kpsa    : Simulated Annealing for Knapsack sub-problem

- kpbf    : Local Search based on bit-flip for Knapsack sub-problem

- tsp2opt : Local Search based on 2-opt for TSP sub-problem

- obj     : Objective function


# Important notes

1. make sure all the python scripts and the files in bins directory are executable:
   chmod +x *.py bins -R

2. due to space constraint, only few TTP instances are included.
  
 TTP instances are available at:
   http://cs.adelaide.edu.au/~optlog/CEC2014COMP_InstancesNew/



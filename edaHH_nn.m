%%%%%%% Hyper-heuristic based on EDA - Estimation of Distribution Algorithm
%%%%%%% using a Bayesian Network as probabilistic graphic model
%%%%%%%
%%%%%%% sub-heuristics for TTP Problem:
%%%%%%% - kpsa    : Simulated Annealing for Knapsack sub-problem
%%%%%%% - kpbf    : Local Search based on bit-flip for Knapsack sub-problem
%%%%%%% - kpsa    : Simulated Annealing for KP component
%%%%%%% - tsp2opt : Local Search based on 2-opt for TSP sub-problem
%%%%%%% - otspswap: Disruptive operator: swap two cities at random
%%%%%%% - otsp4opt: Disruptive operator: double bridge
%%%%%%% - okpbf20 : Disruptive operator: bitflip (20% bits at random)

% 1- Initialization:
%     Generate random N population of individuals of size ns.
% 2- Fitness Evaluation
%     Calculate Fitness based on combination of local searchs and
%     mutations. Due the runtime, we storage all previous individuals
%     found until current population over the
%     generations, so first we compare them to avoid
%     the fitness re-calculation.
% 4- Local Search
%     We use LS to improve the selected solutions.
% 3- Selection
%     We select Nbest individuals to learn and build the probabilistic
%     mode. Nbest=N*nsel
% 4- EDA: Constructing the probabilistic model
%     The PGM is learned and build according bayesian estimation and K2
%     metric.
% 5- EDA: Sampling
%     The  new individuals (Nsmp*N) are sampling according PGM. Equals
%     individuals are accepedt because is easier and fast to sample a larger set of
%     ones and remove the equals.We calculate the fitenss of the sampled
%     population using a polynomial function and sort them. After we select
%     the best Nsurv*N from this popuation and re-calculate the fitness
%     using the real function.
% 6- EDA: Survival
%     We merge the sampled individuals (hhsolution_smp) with the 75% of best individuals from current
%     population(survival_rate) and check the previous individuals found until now to find equals
%     ones and set objective values. After that, we remove equals
%     individuals for composing the next population.


addpath(genpath('./bnmatlab/'));
clear all

% size of individual
ns=6;
% multiplication factor for sampling a population
Nsmp=1000;
survival_rate=0.75;
% 0.5 select 50% of population in selection 
nsel=1;
Nsurv=7;
% size of initial population
N=5;

MaxRunTime=300;
% # of generations
MaxGer=1000;
% # of repetitions
nb_rep = 30;

initial_solution = 'sol_input.txt';
out_sol_filename = 'sol_output.txt';


heuristic=char('kpsa','kpbf','tsp2opt','otspswap','otsp4opt','okpbf20','okpbf30','okpbf40');%'otsp2opt'
heuristic=cellstr(heuristic);
% # of heuristics
nh=size(heuristic,1); 


% %% run for one instance
instances{1}='eil51_n50_bounded-strongly-corr_01.ttp';

%% run many instances

% file = 'instances_online.txt';
% arquivo = fopen(file);
% instances = fscanf(arquivo,'%c');
% fclose(arquivo);
% instances = strread(instances,'%s');

% # of instances
nb_inst = size(instances,1);

% Main Loop
for r = 1:nb_inst
    all_hhsolution=[];
    hhsolution_g=[];
    all_solutions_sur=[];
    sur_flag=0; %flag to save surrogart solutions
    for e=1:nb_rep
        
        
        
        %================
        % MAIN ALGORITHM
        %================
        tic
        instance_name = instances{r};
        display(['===>' instance_name]);

        %============================
        % TTP INITIALIZATION (linux)
        %============================

        folder_name = strtok(char(instance_name), '_');
        command=['java -jar ttpapprox.jar database/TTP1_data/' folder_name '-ttp/ ' instance_name ' 71 10000 60000 SOMETHING ' initial_solution];
        [status res]=dos(command);       
        arquivo = fopen(initial_solution);
        instance_init_solution = fscanf(arquivo,'%c');
        fclose(arquivo);
        
        %====================================
        % TTP INITIALIZATION (windows+linux)
        %====================================
%         sol_init = strcat('initial-solutions/',instance_name,'_sol_input_',int2str(e),'.txt');
%         arquivo = fopen(sol_init);
%         instance_init_solution = fscanf(arquivo,'%c');
%         fclose(arquivo);
%         dlmwrite(initial_solution,instance_init_solution,'delimiter','');%re-write the initial solution

        % debug
        fprintf('/!\\ TTP INITIALIZATION DONE\n');
        toc
        
        equ=0;
        cont=0;
               
        %====================
        % EDA INITIALIZATION
        %====================
        i=1;
        bestsofar(:,ns+1)=0;
        while i<=N
            hhsolution(i,1:ns)=round((nh-1)*rand(1,ns)+1);
            %%% Only different individuals are generated
            if i>1
                pop_cur=hhsolution(1:i-1,:);
                j=1;
                equ=0;
                while j<=size(pop_cur,1) && equ==0
                    if  all(hhsolution(i,1:ns)==pop_cur(j,1:ns))
                        equ=1;
                        cont=[cont i];
                        i=i-1;
                    else
                        j=j+1;
                    end
                end
            end
            i=i+1;
        end
        hhsolution(:,ns+1)=0;
        
        for i=1:N             
                if(hhsolution(i,ns+1)==0)
                    hhsolution = ttpfitness(i,ns,hhsolution,heuristic,instance_name,initial_solution,out_sol_filename);
                    all_hhsolution = [all_hhsolution; hhsolution(i,:)];
                end
                dlmwrite(initial_solution,instance_init_solution,'delimiter','');  %restore the original instance initial solution
         end

        
        % debug
        fprintf('/!\\ EDA HH INITIALIZATION DONE\n');
        toc
        
        tic
        %================
        % EDA: MAIN LOOP
        %================
        for g=1:MaxGer
            
            if toc>=MaxRunTime
                fprintf('MaxTime was reached !\n');
                break
            end
            
            if g>1

                %===============
                % EDA: SURVIVAL
                %===============
                % Sort for merge 75% (best???) of current population with sampled population
                sort_hhsolution= sortrows(hhsolution_g,-(ns+1)); % - because is descendent order
                hhsolution_g= sort_hhsolution(1:round(N*survival_rate),:);
                hhsolution=[hhsolution_g;hhsolution_smp];
                %%% Different ones
                differents=[];
                for i=1:size(hhsolution,1)
                    cont=0;
                    for j=1:size(differents,1)
                        if  hhsolution(i,1:ns)==differents(j,1:ns)
                            cont=cont+1;
                        end
                    end
                    if cont==0
                        differents=[differents;hhsolution(i,:)];
                    end
                end
                hhsolution=differents;
                
                hhsolution= hhsolution(1:Nsurv*N,:);

                % debug
                fprintf('/!\\ EDA SURVIVAL DONE\n');
                toc          
            end
            
            
            %====================
            % FITNESS EVALUATION
            %====================
            net=SMrbfnet(ns,all_hhsolution);
            sur=net(hhsolution(:,1:ns)');
            hhsolution(:,ns+1)=sur';
            % debug
            fprintf('/!\\ EDA FITNESS EVALUATION DONE\n');
            toc
  
            % size (hhsolution)
            [M,I] = max(hhsolution(:,ns+1));
            hhsolution
            bestsofar = hhsolution(I,:)

            
            %===========
            % SELECTION
            %===========
            Nbest = min(N,round(size(hhsolution,1)*nsel));
            % Replace
            sort_hhsolution= sortrows(hhsolution,-(ns+1)); % - because is descendent order
            best_hhsolution= sort_hhsolution(1:Nbest,:);
            
            % Tournament with reposition
            %best_hhsolution=tournament_selection(hhsolution,N,Nbest);
            
            % keep the best so far:
            if best_hhsolution(1,ns+1)>bestsofar(:,ns+1)
                bestsofar=best_hhsolution(1,:);
            end
            
            % debug
            fprintf('/!\\ EDA SELECTION DONE\n');
            toc
            
            
            %=========================================
            % EDA: CONSTRUCTING THE PROBABILITY MODEL
            %=========================================
            discrete_nodes = 1:ns; % # id nodes
            node_sizes = nh*ones(1,ns); % node states
            LS=[];
            data=best_hhsolution(:,1:ns)';
            order=1:ns;
            % Net structure using K2 metric:
            dag_best = learn_struct_K2(data, node_sizes, order, 'verbose', 'yes');
            bnet1 = mk_bnet(dag_best, node_sizes);
            G1 = bnet1.dag;
            %draw_graph(G1);% ans node cordinate
            %title('Net structure - K2 metric')
            
            % CPT - priori of Dirichlet
            for i=1:ns
                %Make a multinomial conditional prob. distrib.(CPT) Mutinomial uses the same kernel as Dirichlet
                bnet1.CPD{i}=tabular_CPD(bnet1, i, 'prior_type', 'dirichlet', 'dirichlet_type', 'BDeu');
            end
            % bayesian estimation for parameters
            bnet1=bayes_update_params(bnet1, data);
            CPT1 = cell(1,ns);
            for i=1:ns
                % violate object privacy
                s=struct(bnet1.CPD{i}); 
                CPT1{i}=s.CPT;
            end
            
            % debug
            fprintf('/!\\ EDA PROBABILITY MODEL CONSTRUCTION DONE\n');
            toc
            
            
            %===============
            % EDA: SAMPLING
            %===============
            fprintf('EDA sampling- %d generation. \n', g );
            ncases = Nsmp*N;
            datasmp = zeros(ns, ncases);
            for m=1:ncases
                datasmp(:,m) = cell2num(sample_bnet(bnet1));
                hhsolution_smp=datasmp';
            end
            hhsolution_smp(:,ns+1)=0;
            hhsolution_g=hhsolution;
            
            % debug
            fprintf('/!\\ EDA SAMPLING DONE\n');
            toc
            
            bestsofar
        end   
        
        
        %=====================
        % save output to file
        %=====================
        fileID = fopen('output/hseda-results.csv','a');
        fprintf(fileID, '%s %d %s\n', instance_name, max(hhsolution_g(:,ns+1)),int2str(bestsofar(1:ns)));
        fclose(fileID);
        
        % write input in txt file
        dlmwrite(strcat('output/solutions/',instance_name,'_sol_input',int2str(e),'.txt'),instance_init_solution,'delimiter','');
        % write all surrogate solutions in cvs file
        dlmwrite(strcat('output/solutions/',instance_name,'_solutions_sur',int2str(e),'.txt'),all_solutions_sur,'delimiter',' ');
        % re-write the initial solution
        dlmwrite(initial_solution,instance_init_solution,'delimiter','');

        max(hhsolution_g(:,ns+1))
        bestsofar
    end  
end

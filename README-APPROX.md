# Markus' algorithms

## usage (for initialization only)

java -jar ttpapprox.jar <dataFolder>/<instFolder> <fileName> 71 10000 60000 SOMETHING <./bins/approx/outFileName>

## example

java -jar ttpapprox.jar database/TTP1_data/eil51-ttp/ eil51_n50_bounded-strongly-corr_01.ttp 71 10000 60000 SOMETHING outsol.txt

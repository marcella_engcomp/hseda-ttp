function hhsolution_sur=ttpfitnessSM(b,i,ns,hhsolution); 
hhsolution_sur=hhsolution(i,1:ns);
hhsolution_sur(:,ns+1)=(hhsolution(i,1:ns)*b(2:end))+b(1);%fitness value from last heuristic applied
%fprintf('fitness of individual %d by surrogate polynomial model:%d.\n',i,hhsolution_sur(:,ns+1));

% Surrogate radia basis approximation
% %https://www.mathworks.com/help/nnet/examples/radial-basis-approximation.html
% Example:
%
% X = -1:.1:1;
% T = [-.9602 -.5770 -.0729  .3771  .6405  .6600  .4609 ...
%       .1336 -.2013 -.4344 -.5000 -.3930 -.1647  .0988 ...
%       .3072  .3960  .3449  .1816 -.0312 -.2189 -.3201];
% plot(X,T,'+');
% title('Training Vectors');
% xlabel('Input Vector P');
% ylabel('Target Vector T');
% 
% eg = 0.02; % sum-squared error goal
% sc = 1;    % spread constant
% net = newrb(X,T,eg,sc);
% 
% plot(X,T,'+');
% xlabel('Input');
% 
% X = -1:.01:1;
% Y = net(X);
% 
% hold on;
% plot(X,Y);
% hold off;
% legend({'Target','Output'})
% 
% %N-dimensional case. X=matrix RxQ and T=matrix SxQ


function net=SMrbfnet(ns,hhsolution); 

X=hhsolution(:,1:ns);
T=hhsolution(:,ns+1);
eg = 0.02; % sum-squared error goal
sc = 1;    % spread constant
net = newgrnn(X',T',sc);
%newrb - Design radial basis network
%newgrnn-Design generalized regression neural network
%newpnn - Design probabilistic neural network
%newrbe - Design exact radial basis network
%perf  = perform(net,T',Y'); mse value
% We use y SPREAD (sc) slightly lower than 1, 
% the distance between input values, in order, to get y function 
% that fits individual data points fairly closely. 
% A smaller spread would fit data better but be less smooth.

% I=hhsolution(50:80,1:ns)';
% Y = net(I);
% Y=Y';

%M=[hhsolution(50:80,:) Y']
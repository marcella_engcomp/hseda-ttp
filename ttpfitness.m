function hhsolution=ttpfitness(i,ns,hhsolution,heuristic,instance_name,initial_solution,out_sol_filename); 

for j=1:ns
    command=['java -jar ttplab-bbox.jar ' char(heuristic(hhsolution(i,j))) ' ' instance_name ' ' initial_solution ' ' out_sol_filename];
    [status,result]=dos(command);
    teste=initial_solution;
    arquivo = fopen(teste);
    input = fscanf(arquivo,'%c');
    fclose(arquivo);
    teste=out_sol_filename; %precisa ler o output sem a primeira linha
    arquivo = fopen(teste);
    output = fscanf(arquivo,'%c');
    fclose(arquivo);
    [b solution]=strtok(output,'_');
    [header b]=strtok(input,'_');
    space=find(result==' ');
    obj_value=str2double(result(space(1)+1:space(2)-1));
    %fprintf('obj_value=%f. j:%d, heuristic=%s \n',obj_value,j,char(heuristic(hhsolution(i,j))));
    input=strcat(header,solution);
    %new input file will be the current output
    input=strcat(header,solution);
    dlmwrite(initial_solution,input,'delimiter','');%write input in txt file
end
hhsolution(i,ns+1)=obj_value;%fitness value from last heuristic applied
fprintf('fitness_individual %d=%f. \n',i,obj_value);

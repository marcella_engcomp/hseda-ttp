% Surrogate polynomial first ordem
% y^=b0+b1x1+b2x2
% y^= 1242.3+323.4x1-54.8x2
% https://courses.cit.cornell.edu/jmueller/Lecture_SurrogateModelTypes.pdf
% find b that satisfies b=pseudoinvX*y e y^=X*b

function b=SMpolynomial(ns,hhsolution); 

X=[ones(size(hhsolution,1),1) hhsolution(:,1:ns)];
Y= hhsolution(:,ns+1);
b=X\Y;
   
 

% Example:
% X =[1 -1 -1;
%     1 1 -1;
%     1 -1 0.6667;
%     1 1 0.6667;
%     1 0 -0.4444;
%     1 0 -0.7222;
%     1 0 0.6667;
%     1 -1 -0.1667;
%     1 1 -0.1667;
%     1 0 -1;
%     1 0 0.9444
%     1 0 -0.1667;
%     1 0 1;
%     1 0.1667 -0.1667];% add 1 at the first line in x to compute b0.
% 
% Y=[1004;
% 1636;
% 852;
% 1506;
% 1272;
% 1270;
% 1269;
% 903;
% 1555;
% 1260;
% 1146;
% 1276;
% 1225;
% 1321];
% b=X\Y; % b is automatically calculated using the pseudo-inverse (the pseudo inverse is the LSE)

%%% This function imlpements a surrogate to claculate the fitenss according
%%% a quadratic model f(x)=c1x^2+c2x+c3, in order to find an individual as
%%% the vertice/maximum point in this curve x*=(-c2/2*c1). To build the
%%% model (quadratic function) we use 3 points selected randomly from the
%%% population. 3 points, 3 equations to find c1,c2 and c3

%%% An Estimation of Distribution Algorithm with Cheap and Expensive Local
%%% Search - Aimin Zhou et. al.,2014

function hhsolution_sur=SMquadratic(hhsolution,ns,nh); 

e=1; %threshold to get value different from 0
for i=1:size(hhsolution,1)
    k=round((size(hhsolution,1)-3)*rand(1,1)+2);%parent randomly selected excluding the 1st and the last
    %parent=round((8)*rand(1,1)+2);
    hhsolution_sur(i,:)=hhsolution(i,:);
    for j=1:ns
        z1=hhsolution(k-1,j);
        z2=hhsolution(k,j);
        z3=hhsolution(k+1,j);
        f1=hhsolution(k-1,ns+1);
        f2=hhsolution(k,ns+1);
        f3=hhsolution(k+1,ns+1);
        if abs(z1-z2)>=e && abs(z2-z3)>=e && abs(z3-z1)>=e
            c1=round((1/(z2-z3))*((f1-f2)/(z1-z2)-(f1-f3)/(z1-z3)));
            c2=round((f1-f2)/(z1-z2)-c1*(z1+z2)/(z1-z3));
            if abs(c1)>=e
               z=abs(round(-c2/(2*c1)));
               if z<nh && z>0
               hhsolution_sur(i,j)=z;
               hhsolution_sur(i,ns+1)=0;
               fprintf('j=%d updated from %d to %d \n',j, hhsolution(i,j),hhsolution_sur(i,j));
               end
            end
        end
    end
end

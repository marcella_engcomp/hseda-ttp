%Initial Solution generator

file = 'instances_online.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s');

% nb of instances
nb_inst = size(instances,1);

nb_rep = 20;% nb of repetitions

% Main Loop
for r = 1:nb_inst
    for e=1:nb_rep
        instance_name = instances{r};
        display(['===>' instance_name]);
        initial_solution = strcat(instance_name,'_sol_input_',int2str(e+10),'.txt');
        %====================
        % TTP INITIALIZATION
        %====================
        folder_name = strtok(char(instance_name), '_');     
        command=['java -jar ttpapprox.jar database/TTP1_data/' folder_name '-ttp/ ' instance_name ' 71 10000 60000 SOMETHING ./initial-solutions/' initial_solution];
        [status, res]=dos(command);
    end
end

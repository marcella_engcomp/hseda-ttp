%%%%%%% Hyper-heuristic based on EDA - Estimation of Distribution Algorithm
%%%%%%% using a Bayesian Network as probabilistic graphic model
%%%%%%%
%%%%%%% sub-heuristics for TTP Problem:
%%%%%%% - kpsa    : Simulated Annealing for Knapsack sub-problem
%%%%%%% - kpbf    : Local Search based on bit-flip for Knapsack sub-problem
%%%%%%% - kpsa    : Simulated Annealing for KP component
%%%%%%% - tsp2opt : Local Search based on 2-opt for TSP sub-problem
%%%%%%% - otspswap: Disruptive operator: swap two cities at random
%%%%%%% - otsp4opt: Disruptive operator: double bridge
%%%%%%% - okpbf20 : Disruptive operator: bitflip (20% bits at random)

% 1- Initialization:
%     Generate random N population of individuals of size ns.
% 2- Fitness Evaluation
%     Calculate Fitness based on combination of local searchs and
%     mutations. Due the runtime, we storage all previous individuals
%     found until current population over the
%     generations, so first we compare them to avoid
%     the fitness re-calculation.
% 4- Local Search
%     We use LS to improve the selected solutions.
% 3- Selection
%     We select Nbest individuals to learn and build the probabilistic
%     mode. Nbest=N*nsel
% 4- EDA: Constructing the probabilistic model
%     The PGM is learned and build according bayesian estimation and K2
%     metric.
% 5- EDA: Sampling
%     The  new individuals (Nsmp*N) are sampling according PGM. Equals
%     individuals are accepedt because is easier and fast to sample a larger set of
%     ones and remove the equals.We calculate the fitenss of the sampled
%     population using a polynomial function and sort them. After we select
%     the best Nsurv*N from this popuation and re-calculate the fitness
%     using the real function.
% 6- EDA: Survival
%     We merge the sampled individuals (hhsolution_smp) with the 75% of best individuals from current
%     population(survival_rate) and check the previous individuals found until now to find equals
%     ones and set objective values. After that, we remove equals
%     individuals for composing the next population.


addpath(genpath('./bnmatlab/'));
clear all

ns=6; %size of individual
Nsmp=100; %multiplication factor for sampling a population
survival_rate=0.75;
nsel=1; %0.5select 50% of population in selection 
Nsurv=7;
N=10; %size of initial population

MaxRunTime=600;
MaxGer=1000;% nb of generations
nb_rep = 1;% nb of repetitions

initial_solution = 'sol_input.txt';
out_sol_filename = 'sol_output.txt';


heuristic=char('kpsa','kpbf','tsp2opt','otspswap','otsp4opt','okpbf20','okpbf30','okpbf40');%'otsp2opt'
heuristic=cellstr(heuristic);
nh=size(heuristic,1); %number of heuristics


%% run many instances

file = 'instances_online.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s');

% nb of instances
nb_inst = size(instances,1);

% Main Loop
for r = 1:nb_inst
    all_hhsolution=[];
    hhsolution_g=[];
    all_solutions_sur=[];
    sur_flag=0; %flag to save surrogart solutions
    for e=1:nb_rep
       
        %================
        % MAIN ALGORITHM
        %================
        tic
        instance_name = instances{r};
        display(['===>' instance_name]);

        %====================
        % TTP INITIALIZATION
        %====================
        sol_init = strcat('initial-solutions/',instance_name,'_sol_input.txt');
        arquivo = fopen(sol_init);
        instance_init_solution = fscanf(arquivo,'%c');
        fclose(arquivo);
        dlmwrite(initial_solution,instance_init_solution,'delimiter','');%re-write the initial solution
        % debug
        fprintf('/!\\ ACO DONE\n');
        
        % debug
        fprintf('/!\\ TTP INITIALIZATION DONE\n');
        toc
        
        equ=0;
        cont=0;

        %====================
        % EDA INITIALIZATION
        %====================
        i=1;
        bestsofar(:,ns+1)=0;
        while i<=N
            hhsolution(i,1:ns)=round((nh-1)*rand(1,ns)+1);
            %%% Only different individuals are generated
            if i>1
                pop_cur=hhsolution(1:i-1,:);
                j=1;
                equ=0;
                while j<=size(pop_cur,1) && equ==0
                    %if (size(find(hhsolution(i,1:ns)==pop_cur(j,1:ns)),2)==ns)
                    %if  (hhsolution(i,1:ns)==pop_cur(j,1:ns))
                    if  all(hhsolution(i,1:ns)==pop_cur(j,1:ns))
                        equ=1;
                        cont=[cont i];
                        i=i-1;
                    else
                        j=j+1;
                    end
                end
            end
            i=i+1;
        end
        hhsolution(:,ns+1)=0;
        
        % debug
        fprintf('/!\\ EDA HH INITIALIZATION DONE\n');
        toc
        
        
        %================
        % EDA: MAIN LOOP
        %================
        for g=1:MaxGer
            
            if toc>=MaxRunTime
                fprintf('MaxTime was reached !\n');
                break
            end
            
            if g>1

                %===============
                % EDA: SURVIVAL
                %===============
                % in case of equal individuals (sampled x previous populations), copy the value of fitness
                % already calculated  

                % keep all individuals, so just different ones has fitness evaluation 
                all_hhsolution = [all_hhsolution;hhsolution_g];
                %%% Different ones
                differents=[];
                for i=1:size(all_hhsolution,1)
                    cont=0;
                    for j=1:size(differents,1)
                        if  all_hhsolution(i,1:ns)==differents(j,1:ns)
                            cont=cont+1;
                        end
                    end
                    if cont==0
                        differents=[differents;all_hhsolution(i,:)];
                    end
                end
                all_hhsolution=differents; %there are some different solutions that have the same fitness...
                for i=1:size(hhsolution_smp,1)
                    for j=1:size(all_hhsolution,1)
                        if all (hhsolution_smp(i,1:ns)==all_hhsolution(j,1:ns))
                            hhsolution_smp(i,ns+1)=all_hhsolution(j,ns+1);
                            i=size(hhsolution_smp,1)+1;
                            break;
                        end
                    end
                end

                %Sort for merge 75% (best???) of current population with sampled
                %population
                sort_hhsolution= sortrows(hhsolution_g,-(ns+1)); %- because is descendent order
                hhsolution_g= sort_hhsolution(1:round(N*survival_rate),:);
                hhsolution=[hhsolution_g;hhsolution_smp];
                %%% Different ones
                differents=[];
                for i=1:size(hhsolution,1)
                    cont=0;
                    for j=1:size(differents,1)
                        if  hhsolution(i,1:ns)==differents(j,1:ns)
                            cont=cont+1;
                        end
                    end
                    if cont==0
                        differents=[differents;hhsolution(i,:)];
                    end
                end
                hhsolution=differents;

                % debug
                fprintf('/!\\ EDA SURVIVAL DONE\n');
                toc

                %===============================
                % SURROGATE FITNESS CALCULATION
                %===============================
                hhsolution_sur=[];
                hhsolution_fit=[];
                net=SMrbfnet(ns,all_hhsolution);
                %b=SMpolynomial(ns,all_hhsolution); %%surrogate polynomial model 
                for i=1:size(hhsolution,1)
                    if(hhsolution(i,ns+1)==0)                       
                        hhsolution_sur=[hhsolution_sur; hhsolution(i,:)];
                    else
                        hhsolution_fit=[hhsolution_fit; hhsolution(i,:)];
                    end
                end
                sur=net(hhsolution_sur(:,1:ns)');
                hhsolution_sur(:,ns+1)=sur';
                %hhsolution_sur=hhsolution_sur(1:min(size(hhsolution_sur,1),(Nsurv*N)),:);
    %                 psur=0.1;
    %                 Nbest = round(size(hhsolution_sur,1)*psur); %Nbest solutions will have the fitness recalculated
                %Nbest=Nsurv*N;
                hhsolution_sur= sortrows(hhsolution_sur,-(ns+1)); %- because is descendent order
                solutions_sur=hhsolution_sur(1:Nsurv*N,:); % used in the comparison surrogated X real fitness
                hhsolution_sur(:,ns+1)=0;
                hhsolution= [hhsolution_fit;hhsolution_sur(1:Nsurv*N,:)];
                sur_flag=1; 

                % debug
                fprintf('/!\\ SURROGATE FITNESS DONE\n');
                toc
            
            end
            
            
            %====================
            % FITNESS EVALUATION
            %====================
            for i=1:size(hhsolution,1)              
                if(hhsolution(i,ns+1)==0)
                    hhsolution = ttpfitness(i,ns,hhsolution,heuristic,instance_name,initial_solution,out_sol_filename);
                    all_hhsolution = [all_hhsolution; hhsolution(i,:)];
                end
                dlmwrite(initial_solution,instance_init_solution,'delimiter','');  %restore the original instance initial solution
            end
            
            if sur_flag==1
             solutions_sur(:,ns+2)=hhsolution(size(hhsolution_fit,1)+1:size(hhsolution,1),ns+1);%columns ns+2 contains the real value
             all_solutions_sur=[all_solutions_sur;solutions_sur];
            end
            
            % debug
            fprintf('/!\\ EDA FITNESS EVALUATION DONE\n');
            toc
            
            
            %size (hhsolution)
            [M,I] = max(hhsolution(:,ns+1));
            hhsolution
            bestsofar = hhsolution(I,:)



            %===========================
            % LOCAL SEARCH: replace op.
            %===========================
            k=false;
            pls=0.1; % LS probability
            ls_hhsolution=[];
            
            % loop all population solutions
            popsize = size(hhsolution,1);
            for  i=1:popsize
                if rand(1)>pls
                    continue
                end
                
                % LS main loop
                improve = 1;
                ls_it = 0;
                while improve==1 % repeat while there is improvement
                    ls_it = ls_it+1;
                    %improve = 0;
                    new_solution=hhsolution(i,:);
                    
                    % random select sub-heuristics
                    %for h=1:nh
                    h=round((nh-1)*rand(1)+1);
                        
                        % random select solution variables
                        %for n=1:ns
                        n=round((ns-1)*rand(1)+1);
                            
                            if new_solution(:,n)==h
                                continue
                            end
                            improve = 0;
                            
                            % generate the neigbor
                            backup = new_solution(:,n);
                            new_solution(:,n)=h;
                            
                            %=====================
                            % TODO: is it possible to simplify this further ?
                            for j=1:size(all_hhsolution,1)
                                if all (new_solution(1,1:ns)==all_hhsolution(j,1:ns))
                                    new_solution(1,ns+1)=all_hhsolution(j,ns+1);
                                    %fprintf('New solution from LS already exists.\n');
                                    k=true;
                                    break;
                                else
                                    k=false; % test if the individual belong to all_hhsolution
                                end
                            end
                            if k==true
                                continue
                            end
                            %=====================
                            
                            new_solution = ttpfitness(1,ns,new_solution,heuristic,instance_name,initial_solution,out_sol_filename);
                            all_hhsolution = [all_hhsolution; new_solution];
                            %fprintf('New solution by LS.\n');                      
                            ls_hhsolution = [ls_hhsolution;new_solution];
                            
                            if new_solution(:,ns+1) > bestsofar(:,ns+1) 
                                bestsofar = new_solution;
                                % mark as improved and get out
                                improve = 1;
                                break 
                            end
                            
                            % get back to original
                            new_solution(:,n)=backup;
                            
                        %end % end loop ns
                        
                        if improve==1
                            break 
                        end
                    %end % end loop nh
                    
                    fprintf('=> %d : bsf=%d | lsbest=%d\n', ls_it, bestsofar(:,ns+1), new_solution(:,ns+1));
                end % end main LS loop
                
            end % end loop population
            hhsolution=[hhsolution;ls_hhsolution];%add the ls solutions to the current population
            
            % debug
            fprintf('/!\\ LOCAL SEARCH DONE\n');
            toc 
            
            %===========
            % SELECTION
            %===========
            Nbest = min(N,round(size(hhsolution,1)*nsel));
            % Replace:
            sort_hhsolution= sortrows(hhsolution,-(ns+1)); %- because is descendent order
            best_hhsolution= sort_hhsolution(1:Nbest,:);
            %best_hhsolution=sort_hhsolution;
            
            % keep the best so far:
            if best_hhsolution(1,ns+1)>bestsofar(:,ns+1)
                bestsofar=best_hhsolution(1,:);
            end
            
            % debug
            fprintf('/!\\ EDA SELECTION DONE\n');
            toc
            
            
            %=========================================
            % EDA: CONSTRUCTING THE PROBABILITY MODEL
            %=========================================
            discrete_nodes = 1:ns; %num id nodes
            node_sizes = nh*ones(1,ns);%node states
            LS=[];
            data=best_hhsolution(:,1:ns)';
            order=1:ns;
            % Net structure using K2 metric:
            dag_best = learn_struct_K2(data, node_sizes, order, 'verbose', 'yes');
            bnet1 = mk_bnet(dag_best, node_sizes);
            G1 = bnet1.dag;
            %draw_graph(G1);% ans node cordinate
            %title('Net structure - K2 metric')
            
            % CPT - priori of Dirichlet
            for i=1:ns
                bnet1.CPD{i}=tabular_CPD(bnet1, i, 'prior_type', 'dirichlet', 'dirichlet_type', 'BDeu');%Make a multinomial conditional prob. distrib.(CPT) Mutinomial uses the same kernel as Dirichlet
                %bnet1.CPD{i}=tabular_CPD(bnet1, i); % random
            end
            %bnet1 = learn_params(bnet1, dados);% likewood estimation to complete data over random CPT
            bnet1=bayes_update_params(bnet1, data); %bayesian estimation for parameters
            CPT1 = cell(1,ns);
            for i=1:ns
                s=struct(bnet1.CPD{i});  % violate object privacy
                CPT1{i}=s.CPT;
            end
            
            % debug
            fprintf('/!\\ EDA PROBABILITY MODEL CONSTRUCTION DONE\n');
            toc
            
            
            %===============
            % EDA: SAMPLING
            %===============
            fprintf('EDA sampling- %d generation. \n', g );
            ncases = Nsmp*N;
            datasmp = zeros(ns, ncases);
            for m=1:ncases
                datasmp(:,m) = cell2num(sample_bnet(bnet1));
                hhsolution_smp=datasmp';
            end
            hhsolution_smp(:,ns+1)=0;
            %hhsolution_g=best_hhsolution;
            hhsolution_g=hhsolution;
            %final_hhsolution.ger{g}=hhsolution_g;
            
            % debug
            fprintf('/!\\ EDA SAMPLING DONE\n');
            toc
            
            
            bestsofar
        end   
                 
        
        %=====================
        % save output to file
        %=====================
        fileID = fopen('output/results.csv','a');
        fprintf(fileID, '%s %d %s\n', instance_name, max(hhsolution_g(:,ns+1)),int2str(bestsofar(1:ns)));
        fclose(fileID);
        
        dlmwrite(strcat('output/',instance_name,'_sol_input',int2str(e),'.txt'),instance_init_solution,'delimiter','');%write input in txt file
        
        dlmwrite(strcat('output/',instance_name,'_solutions_sur',int2str(e),'.txt'),all_solutions_sur,'delimiter',' ');%write all surrogate solutions in cvs file
        %C=dlmread(strcat('output/',instance_name,'_solutions_sur',int2str(e),'.txt')); %read the fiele
       
        %%%%%%%%
        %dlmwrite(initial_solution,instance_init_solution,'delimiter','');%re-write the initial solution
        %%%%%%%%
        max(hhsolution_g(:,ns+1))
        bestsofar
    end  
end

# Hyper-heuristic based on EDA for the TTP


## Descriptive

Estimation of Distribution Algorithm as a hyper-heuristic using a Bayesian 
Network as probabilistic graphic model for the travelling Thief Problem.

## Prerequisites:
- java 1.7+
- a linux OS / Windows will work for most heuristics
- MATLAB
- R-3.2.3+
- irace-2.1+
- reach

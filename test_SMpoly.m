% This code is used to test the surrogate accurate 
%1st column surrogate fitness, 2nd column real fitness

% instances{1}='berlin52_n255_bounded-strongly-corr_05.ttp';
% r=1;
% e=1;
% instance_name = instances{r};
% C=dlmread(strcat('output/',instance_name,'_solutions_sur',int2str(e),'.txt'));
% [sort_sur, is]=sortrows(C,-7);
% [sort_real,ir]=sortrows(C,-8);
% B=[is ir];


%% run many instances

N=10;

file = 'instances_online.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s');

% nb of instances
nb_inst = size(instances,1);
% nb repetitions
nb_rep=1;
for r = 1:nb_inst
    for e=1:nb_rep
        instance_name = instances{r};
        C.itc{r}.exec{e}=dlmread(strcat('output/',instance_name,'_solutions_sur',int2str(e),'.txt'));
    end
end

for r = 1:nb_inst
    for e=1:nb_rep
        [sort_sur, is]=sortrows(C.itc{r}.exec{e},-7);
        [sort_real,ir]=sortrows(C.itc{r}.exec{e},-8);
        B.itc{r}.exec{e}=[is(1:N) ir(1:N)];% N best solutions
        %count the number of solutions are in the both set, is and ir (10 best solutions)
        for i=1:size(B.itc{r}.exec{e}(:,1),1)
            for j=1:size(B.itc{r}.exec{e}(:,2),1)
                if  B.itc{r}.exec{e}(i,1)==B.itc{r}.exec{e}(j,2)
                    cont=cont+1;
                    break
                end
            end
        end
        B.itc{r}.cont{e}=cont;
    end
end




% Give here the folder where you have uncompressed the Structure Learning Package for BNT
if isunix
SLP_HOME = '/mnt/win_data/Olivier/Matlab/BNT_SLP/';
else
SLP_HOME = 'D:/Olivier/Matlab/BNT_SLP';
end

folders = {'learning','scoring','misc','examples','datas','working','working/modded_LinkStrength','working/elr','working/score_dag.c'};
addpath(SLP_HOME)
for f=1:length(folders)
  addpath(fullfile(SLP_HOME, folders{f}))
end
clear SLP_HOME f folders